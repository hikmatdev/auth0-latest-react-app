import React, { Component } from "react";

import "./App.css";
import Main from "./components/main";
import Secret from "./components/secret";
import NotFound from "./components/notFound";
import CallBack from "./components/callBack";

class App extends Component {
  render() {
    let mainComponent = "";
    switch (this.props.location) {
      case "":
        mainComponent = <Main {...this.props} />;
        break;
      case "callback":
        mainComponent = <CallBack />;
        break;
      case "secret":
        mainComponent = this.props.auth.isAuthenticated() ? (
          <Secret {...this.props} />
        ) : (
          <NotFound />
        );
        break;

      default:
        mainComponent = <NotFound />;
    }
    return (
      <div className="App">
        <div>{mainComponent}</div>
      </div>
    );
  }
}

export default App;
