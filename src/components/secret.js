import React, { Component } from "react";

class Secret extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <h1>Secret</h1>
        <p>Welcome to react app Mr. {this.props.name}</p>
        
        <br />
        <p>This is a secret area</p>
        <a href="/">home</a>
        <hr />
        <button onClick={this.props.auth.logout}> Logout</button>
      </div>
    );
  }
}

export default Secret;
