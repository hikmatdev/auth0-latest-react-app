import React, { Component } from "react";

class NotFound extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <h1>Not found</h1>

        <a href="/"> home</a>
      </div>
    );
  }
}

export default NotFound;
