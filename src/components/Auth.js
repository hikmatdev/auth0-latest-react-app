/* eslint no-restricted-globals: 0 */

import auth0 from "auth0-js";
import jwtDecode from "jwt-decode";
import Auth0Lock from "auth0-lock";

const LOGIN_SUCCESS_PAGE = "/secret";
const LOGIN_FAILURE_PAGE = "/";

var globalProfile;
var globalToken;

const clientID = "GACCGcwUNnsq3upQKdIrGrUVwWFLvDnF";
const domain = "alert-amigo.auth0.com";
var options = {
  redirectUri: "http://localhost:3000",
  audience: "https://alert-amigo.auth0.com/userinfo",
  allowSignUp: false
};
var lock = new Auth0Lock(clientID, domain, options);
export default class Auth {
  auth0 = new auth0.WebAuth({
    domain: "alert-amigo.auth0.com",
    clientID: "GACCGcwUNnsq3upQKdIrGrUVwWFLvDnF",
    redirectUri: "http://localhost:3000/callback",
    audience: "https://alert-amigo.auth0.com/userinfo",
    responseType: "token id_token",
    scope: "openid profile"
  });

  constructor() {
    this.login = this.login.bind(this);
  }

  login() {
    /*    lock.show(function(err, profile, token) {
      if (err) {
        return alert(err);
      }

      globalProfile = profile;
      globalToken = token;

      localStorage.setItem("token", globalToken);
      localStorage.setItem("profile", globalProfile);

    }); */
    this.auth0.authorize();
  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResults) => {
      if (authResults && authResults.accessToken && authResults.idToken) {
        let expiresAt = JSON.stringify(
          authResults.expiresIn * 1000 + new Date().getTime()
        );
        localStorage.setItem("access_token", authResults.accessToken);
        localStorage.setItem("id_token", authResults.idToken);
        localStorage.setItem("expires_at", expiresAt);
        location.hash = "";
        location.pathname = LOGIN_SUCCESS_PAGE;
      } else if (err) {
        location.pathname = LOGIN_FAILURE_PAGE;
        console.log(err);
      }
    });
  }

  isAuthenticated() {
    let expiresAt = JSON.parse(localStorage.getItem("expires_at"));
    return new Date().getTime() < expiresAt;
  }

  logout() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    location.pathname = LOGIN_FAILURE_PAGE;
  }

  getProfile() {
    if (localStorage.getItem("id_token")) {
      return jwtDecode(localStorage.getItem("id_token"));
    } else {
      return {};
    }
  }
}
