import React, { Component } from "react";

class Main extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <h1>Home</h1>
        <p>Welcome to react app {this.props.name}</p>
        <br />
        <p>
          So you want to goto secret area <a href="/secret"> Click here!</a>
        </p>
        {!this.props.auth.isAuthenticated() && (
          <div>
            Please login:
            <hr />
            <button onClick={this.props.auth.login}>login</button>
          </div>
        )}
      </div>
    );
  }
}

export default Main;
